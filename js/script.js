let num1 = 0;
let num2 = 0;
let operator = null;

$(document).ready(function() {

    $("#ce").on("click", function() {
        borrar();
    });

    $("#c").on("click", function() {
        borrar();
    });

    $("#cero").on("click", function() {
        $("#resultado").append("0");
    });

    $("#uno").on("click", function() {
        $("#resultado").append("1");
    });

    $("#dos").on("click", function() {
        $("#resultado").append("2");
    });

    $("#tres").on("click", function() {
        $("#resultado").append("3");
    });

    $("#cuatro").on("click", function() {
        $("#resultado").append("4");
    });

    $("#cinco").on("click", function() {
        $("#resultado").append("5");
    });

    $("#seis").on("click", function() {
        $("#resultado").append("6");
    });

    $("#siete").on("click", function() {
        $("#resultado").append("7");
    });

    $("#ocho").on("click", function() {
        $("#resultado").append("8");
    });

    $("#nueve").on("click", function() {
        $("#resultado").append("9");
    });

    $("#suma").on("click", function() {
        num1 = $("#resultado").html();
        operator = "+";
        vaciar();
    });

    $("#resta").on("click", function() {
        num1 = $("#resultado").html();
        operator = "-";
        vaciar()
    });

    $("#multiplicacion").on("click", function() {
        num1 = $("#resultado").html();
        operator = "*";
        vaciar()
    });

    $("#division").on("click", function() {
        num1 = $("#resultado").html();
        operator = "/";
        vaciar()
    });

    $("#igual").on("click", function() {
        num2 = $("#resultado").html();
        resultado();
    });

});

function borrar() {

    $("#resultado").html("");
    num1 = 0;
    num2 = 0;
    operator = "";

}

function resultado() {

    let resultado = 0;

    switch(operator) {

        case "+":
        resultado = parseFloat(num1) + parseFloat(num2);
        break;

        case "-":
        resultado = parseFloat(num1) - parseFloat(num2);
        break;

        case "*":
        resultado = parseFloat(num1) * parseFloat(num2);
        break;

        case "/":
        resultado = parseFloat(num1) / parseFloat(num2);
        break;

    }

    borrar();
    $("#resultado").html(resultado);

}

function vaciar(){

    $("#resultado").html("");
    
}